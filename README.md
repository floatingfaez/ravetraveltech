# rave-travel-tech

> Rave is a revolutionary new way to book travel and hospitality services. We focus on user experience to deliver a great shopping experience. Webiste: https://www.raveit.io/

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
